Name: bughunting-tasks
Version: 2019.10
Release: 1%{?dist}
Summary: Tasks for Bughunting

License: MIT
URL: https://gitlab.com/bughunting/tasks
Source0: %{name}-%{version}.tar.bz2
Source1: dependencies.txt

ExclusiveArch: x86_64

Requires: bughunting

Requires: bash
Requires: bzip2
Requires: coreutils
Requires: cpio
Requires: diffutils
Requires: fedora-release
Requires: findutils
Requires: gawk
Requires: gcc
Requires: gcc-c++
Requires: grep
Requires: gzip
Requires: info
Requires: make
Requires: patch
Requires: redhat-rpm-config
Requires: rpm-build
Requires: sed
Requires: shadow-utils
Requires: tar
Requires: unzip
Requires: util-linux
Requires: which
Requires: xz
Requires: attr
Requires: autoconf
Requires: automake
Requires: bison
Requires: gettext
Requires: sqlite
Requires: strace
Requires: texinfo

# Generated requires.  Be careful here, as of my last checking (~2015) RPM has
# 80K bytes limit which any macro can expand into.
%(cat %{SOURCE1} | sed 's|^|Requires: |')

%description
Bughunting tasks


%prep
%setup -q


%install
make install DESTDIR=%{buildroot}


%files
%doc README.md
%license LICENSE
%{_sharedstatedir}/bughunting/tasks/*


%changelog
* Thu Oct 03 2019 Pavel Valena <pvalena@redhat.com> - 2019.10-1
- Initial package.
