How to prepare tasks for Bughunting 2019
========================================

Usual workflow is:
* (a) install the bughunting server and clients
* (b) prepare the task
* (c) check whether the check works
* (d) prepare a patch with correct solution for further reference
* (e) prepare a pull-requst on gitlab, check CI


(a) Install the bughunting server/clients
-----------------------------------------

Install 'bughunting-server' from
[https://copr.fedorainfracloud.org/coprs/pvalena/bughunting/]

```
  $ sudo dnf copr enable pvalena/bughunting
  $ sudo dnf install bughunting{,server,client}
```

If you don't want to install and run bughunting server on your machine, there
is an analternate way how to develop tasks for Bughunting in a container.
For that variant, follow instructions at [TBD].


(b) Add a new task
------------------

Usually, this should be sufficient.

```
  $ cp -r  warm_up  YOUR_TASK_NAME
  $ vim YOUR_TASK_NAME/task.yml
  $ :                     # edit task contents, including `dependencies` array
  $ make check            # perform basic (static) auto-QA
```

[1] One *important* note here:  Hunters do not have machines connected to
    the Internet.  This should be taken into account when the task difficulty is
    designed.  We usually do not block installing manuals like manual pages or
    '*-doc*' subpackages.  Feel free to adjust DEPLIST with useful documentation
    packages.


(c) Check that the task works
-----------------------------

* In separate terminal (or in background) run _under root_ the command

  $ sudo huntserver --local
or
  $ sudo huntserver --local &> huntserver.log &

* In another terminal _under regular user_:

```
  $ cd YOUR_TASK_NAME
  $ huntlocal setup       # setup
  $ huntlocal             # this should say "failed" (not fixed yet)
  $ cd work.tmp           # go to temporary dir
  $ :                     # fix that bug there ;-)
  $ cd ..                 # go back
  $ huntlocal             # now it should say "success"
  $ huntlocal patch       # show and save the fix
  $ huntlocal clean       # cleanup
```

In case you don't see what you expect (e.g. the file in work.tmp is fixed but you
don't get any points), then it might be worth looking what exactly the server did.

For that, you can use verbose mode: `huntlocal -v`

Alternatively, find the sandbox directory in the bughunting server that runs locally.
The below is an example of the log of the bughunting server that is running
locally, and you see the sendbox directory is `/tmp/bughunting20190312-24752-1rf5yjl`:

```
  === Starting local test ===
  task: /home/hhorak/bughunting/bhtasks-2019/warm_up2
  work: /home/hhorak/bughunting/bhtasks-2019/warm_up2/work.tmp
  check: /tmp/bughunting20190312-24752-1rf5yjl

```

You can go to that directory and see the log of the check script, you can run
and debug it from there as well:

```
  $ cd /tmp/bughunting20190312-24752-1rf5yjl
  $ cat check.log
  $ cd work
  $ sudo -u bughunting1 ./check.sh
   ...
```

(d) Prepare patch with correct solution for further reference
-------------------------------------------------------------

The patch with correct solution will also be helpful in potential discussions whether the user fixed the issue or not.

Put it into root directory of the task, e.g. ./warm_up/fix.patch

```
    $ huntlocal patch > fix.patch
    $ :                             # add fix.patch to task.yml
    $ huntlocal verify              # verify the patch works [TBD]
```

(e) Open a pull-request
-----------------------
It is preferred to create a pull-request and check CI results.

Before pushing to master, pull latest changes and REBASE them onto your local clone:

    $ git pull --rebase origin master
